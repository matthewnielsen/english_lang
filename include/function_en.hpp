#ifndef FUNCTION_EN_HPP
#define FUNCTION_EN_HPP

#include <vector>
#include <string>

void define_function(
  std::string filename,
  std::vector<std::string> name,
  std::vector<std::string> takes,
  std::vector<std::string> returns);

#endif