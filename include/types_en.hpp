#ifndef TYPES_EN_HPP
#define TYPES_EN_HPP

enum Verb{
  write,
  perform,
  function,
  returns,
  takes,
  does_cond,
  put,
  add,
  subtract,
  multiply,
  divide,
  if_cond,
  elseif_cond,
  else_cond,
  end_cond,
  set,
  create,
  for_loop,
  unknown_verb};

enum Identifier{ to, equals, unknown_identifier };

#endif