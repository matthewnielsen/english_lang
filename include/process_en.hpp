#ifndef PROCESS_EN_HPP
#define PROCESS_EN_HPP

#include <vector>
#include <string>

void process_tokens(std::vector<std::string> tokens, std::string output_file_name);

#endif